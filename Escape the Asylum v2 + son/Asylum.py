from tkinter import *
from playsound import playsound
import winsound

def jeuprinc():
    accueil.destroy()
    winsound.PlaySound(None, winsound.SND_FILENAME)
    jeu = Tk()
    jeu.configure(bg='black')
    #Les différentes positions du joueur
    global map1,map2,map3,map4,map5,map6,map7,map8,map9,flechegauche,flechedroite,flechehaut,flechebas,ok
    global inventaire,ptvie1,ptvie3,text,canvasmap,canvasinv,canvasviegauche,canvasvie2

    map0 = PhotoImage(file="Image/map0.png")
    map1 = PhotoImage(file="Image/map1.png")
    map2 = PhotoImage(file="Image/map2.png")
    map3 = PhotoImage(file="Image/map3.png")
    map4 = PhotoImage(file="Image/map4.png")
    map5 = PhotoImage(file="Image/map5.png")
    map6 = PhotoImage(file="Image/map6.png")
    map7 = PhotoImage(file="Image/map7.png")
    map8 = PhotoImage(file="Image/map8.png")
    #L'interface
    flechegauche= PhotoImage(file="Image/flechegauche.png")
    flechedroite= PhotoImage(file="Image/flechedroite.png")
    flechehaut= PhotoImage(file="Image/flechehaut.png")
    flechebas= PhotoImage(file="Image/flechebas.png")

    ok= PhotoImage(file="Image/ok.png")

    inventaire=PhotoImage(file="Image/inventaire.png")
    flingue=PhotoImage(file="Image/flingue.png")
    clee=PhotoImage(file="Image/clef.png")
    couteau=PhotoImage(file="Image/couteau.png")
    troussee=PhotoImage(file="Image/trousse.png")
    minimap=PhotoImage(file="Image/minimap.png")

    ptvie1=PhotoImage(file="Image/vie.png")
    ptvie3=PhotoImage(file="Image/vie-1.png")

    jeu.title("Escape the Asylum")
    #Centrer la fenetre au milieu
    l=1360
    h=920
    largeur=jeu.winfo_screenwidth()
    hauteur=jeu.winfo_screenheight()
    x=(largeur/2)-(l/2)
    y=(hauteur/2)-(h/2)
    jeu.geometry("%dx%d+%d+%d" % (l,h,x,y))

    canvasmap = Canvas(jeu,width=850, height=550, highlightthickness=0)

    canvasinv = Canvas(jeu,width=300, height=90, highlightthickness=0)
    canvasinv.create_image(0, 0, anchor=NW, image=inventaire)
    canvasinv.place(x=520, y=655)

    canvasflingue = Canvas(jeu,width=37, height=39, highlightthickness=0)
    canvascle = Canvas(jeu,width=37, height=39, highlightthickness=0)
    canvascouteau = Canvas(jeu,width=37, height=39, highlightthickness=0)
    canvastrousse = Canvas(jeu,width=37, height=39, highlightthickness=0)
    canvasminimap = Canvas(jeu,width=37, height=39, highlightthickness=0)

    canvasviegauche = Canvas(jeu, width=48, height=49, highlightthickness=0)
    canvasviegauche.create_image(0, 0, anchor=NW, image=ptvie1)
    canvasviegauche.place(x=610, y=750)
    canvasvie2 = Canvas(jeu, width=48, height=49, highlightthickness=0)
    canvasvie2.create_image(0, 0, anchor=NW, image=ptvie1)
    canvasvie2.place(x=680, y=750)

    winsound.PlaySound('Sons/normal.wav', winsound.SND_FILENAME|winsound.SND_ASYNC|winsound.SND_NOSTOP)

    #Scénarios
    arrivee = "Chef de la gendarmerie du village local, vous avez entendu parler d'une affaire étrange\n\nEn fin de service, vous décidez de vous rendre sur le lieu en question...\n\nArrivé devant ce grand bâtiment, vous y aperçevez une pancarte et arrivez à distinguer :\n\nAsile P ych  tr  ue\n\nVous passez alors le portail grinçant qui surplombe cet asile\nLa porte principale étant ouverte, vous décidez d'entrer..."
    carte = "En entrant vous remarquez qu'une odeur répugnante de latrine règne\n\nSur le mur droit du petit couloir d'entrée vous y trouvez la carte de cet asile abandonné\n\nDésirez vous la prendre ?"
    middle = "Froussard que vous êtes,\nvous avez peur de l'obscurité et n'osez\npas revenir sur vos pas\n\nVous serez alors guidé le long de l'expédition\n\nDirigez vous avec les flèches"
    devantarbre = "Vous voilà devant un arbre\n\nBloquant le chemin d'en face,\nVous n'avez pas le choix d'aller à droite \nou à gauche"
    gaucheg = "Alors que vous commencez à stresser\n\nVoilà le premier choix qui s'offre à vous\n\nLaissez vous guider par votre instinct\n\n\nLa salle de réunion ?\nLa salle des équipiers ?\nOu bien avez vous peur de vous y aventurer.."
    droited = "Alors que vous commencez à stresser\n\nVoilà le premier choix qui s'offre à vous\n\nLaissez vous guider par votre instinct\n\n\nLe local ?\nLe réfectoire ?\nOu bien avez vous peur de vous y aventurer.."
    reusanspisto = "Vous prenez votre courage à deux mains\net décidez d'entrer dans cette pièce\n\nSur la table, vous trouvez une clé\n\n\nVous n'avez pas encore été fouiller \nla salle équipier\nVous pouvez aussi partir... à vous de choisir"
    reuavecpisto = "Vous entrer doucement dans la salle\n\nSur la table vous trouvez une clé\n\nVous pouvez désormais partir et\ncontinuer dans le couloir"
    equisanscle = "Vous prenez votre courage à deux mains\net décidez d'entrer dans cette pièce\n\nDans la salle, vous remarquer des casiers\nUn casier n'a pas de cadenas, vous l'ouvrer\n\nDans le casier, vous trouvez un pistolet\nN'étant pas en service, vous n'avez pas d'arme\net décidez de prendre ce pistolet\nen cas d'urgence\n\n\nVous n'avez pas encore été fouiller \nla salle de réunion\nVous pouvez aussi partir... à vous de choisir"
    equiaveccle = "Vous entrer dans cette salle\n\nPlus loin dans la pièce sombre,\nvous aperçevez des casiers\n\nUn casier n'a pas de cadenas, vous l'ouvrer\n\nDans le casier, vous trouvez un pistolet\nN'étant pas en service, vous n'avez pas d'arme\net décidez de prendre ce pistolet\nen cas d'urgence\n\n\nVous pouvez désormais partir et\ncontinuer dans le couloir" 
    localavecpain = "Malheureusement, ou heureusement\nCela depend de votre point de vue,\nvous ne trouvez rien dans ce local\n\n\nVous pouvez donc continuer votre chemin"
    localsanspain = "Malheureusement, ou heureusement\nCela depend de votre point de vue,\nvous ne trouvez rien dans ce local\n\n\nTenter le réfectoire ? ou continuer ?\n\nVous avez le choix"
    refsansloc = "Voici donc la première salle dans laquelle\nvous décidez de rentrer..\n\nN'ayant pas mangé ce soir,\nvous avez drôlement faim\n\nCela tombe bien vous tombez sur \nun morceau de pain, un peu dur..\nMais vous le mangez tout de même\n\n\nVous sortez de cette salle et avez le choix :\n\nEssayer d'entrer dans le local\nou\nContinuer la route dans le couloir sombre"
    refavecloc = "A peine rentré dans le réfectoire,\nvous entendez votre ventre crier famine\n\nPlus loin, vous trouvez justement\nun morceau de pain, un peu dur..\nMais vous le mangez tout de même\n\n\nVous sortez de cette salle et décidez de\ncontinuer dans le couloir"
    ascen = "Un escalier bloqué par des lits,\nun ascenseur hors service,\net pour couronner le tout,\nun néon du plafond qui grésille\n\n\nTout ces éléments commence à vous\nfaire froid dans le dos\n\nAvancez donc"
    wcgauchefirst = "Des toilettes !\nFermées évidement..\n\nCependant sur le mur, vous voyez quelque chose\n\nC'est une trousse de secours médical !\n\nVous la prenez bien-sûr !"
    fou1 = "Vous continuer dans le couloir\nretombez au niveau de l'arbre géant\n\nUne grande porte se dresse devant cet arbre\nVous entendez chuchoter derrière la porte\n\nMunis de votre pistolet,\nvous visez tel que l'on vous a appris\n\nDéterminé et gonflé d'un élan d'adrénaline,\nvous entrez\n\nUn psychopathe vous saute dessus,\npistolet chargé, vous tirez sans hésiter\n\nUn peu choqué des évenement\nvous décidez de sortir de cet asile\n\nPetit bémol, la porte de l'entrée a été\nfermée à clé certainement par le fou\nAllez donc trouver la clé pour sortir"
    fou2 = "Vous continuer dans le couloir\nretombez au niveau de l'arbre géant\n\nUne grande porte se dresse devant cet arbre\nVous entendez chuchoter derrière la porte\n\nMunis de votre couteau,\nvous vous placez en position defensive\n\nDéterminé et gonflé d'un élan d'adrénaline,\nvous entrez\n\nUn psychopathe vous saute dessus,\ncouteau paré,\nvous lui mettez un coup mortel dans l'estomac\n\nUn peu choqué des évenement\nvous décidez de sortir de cet asile\n\nPetit bémol, la porte de l'entrée a été\nfermée à clé certainement par le fou\nAllez donc trouver la clé pour sortir"
    fou3 = "Vous continuer dans le couloir\nretombez au niveau de l'arbre géant\n\nUne grande porte se dresse devant cet arbre\nVous entendez chuchoter derrière la porte\n\nMunis de votre couteau,\nvous vous placez en position defensive\n\nDéterminé et gonflé d'un élan d'adrénaline,\nvous entrez\n\nUn psychopathe vous saute dessus,\ncouteau paré,\nvous lui mettez un coup mortel dans l'estomac\n\nUn peu choqué des évenement\nvous décidez de sortir de cet asile\n\nDans l'arbre vous y aperçevez un espace\net vous y faufilez"
    fou4 = ""
    fou5 = ""
    fou6 = ""
    fou7 = ""
    fou8 = ""
    fou9 = ""

    #Variables qlqconques
    global clef,pistolet,pain,scalpel,trousse
    clef,pistolet,pain,scalpel,trousse = 0,0,0,0,0
    global reunion,equipier,refectoir,locale,fou,vie
    reunion,equipier,refectoir,locale,fou,vie = 0,0,0,0,0,0

    text = Label(jeu, text=arrivee, font='size, 20', bg='black', fg="white", justify = CENTER)
    text.place(x=100, y=150)


    #Déroulement de l'histoire
    def effacermap():
        global canvasmap
        canvasmap.destroy()
    def vide():
        pass
    def premiertexte(): #texte carte
        text.config(text=carte, font='size, 20')
        boutonok.config(command=deuxiemetexte)        
    def deuxiemetexte(): #texte middle
        remisemap0()
        playsound('Sons/carte.wav')
        canvasminimap.create_image(0, 0, anchor=NW, image=minimap)
        canvasminimap.place(x=779.5, y=699)        
        text.config(text=middle, font='size, 15',bd=1, width=37)
        text.place(x=855, y=90)
        boutonhaut.config(command=troisiemetexte)
        boutonok.config(command=vide)
    def troisiemetexte(): #texte devantarbre
        remisemap1()
        playsound('Sons/pas.wav')
        text.config(text=devantarbre)
        boutongauche.config(state="normal", command=gauche)
        boutondroite.config(state="normal", command=droit)
        boutonhaut.config(state=DISABLED)
        boutonbas.config(state=DISABLED)
        boutonok.config(command=vide)
    def gauche(): #texte gaucheg
        remisemap2()
        playsound('Sons/pas.wav')
        text.config(text=gaucheg)
        boutongauche.config(state="normal", command=gauchegauche)
        boutonhaut.config(state="normal", command=sallereunion)
        boutonbas.config(state="normal", command=salleequipier)
        boutondroite.config(state=DISABLED)
        boutonok.config(command=vide)
    def sallereunion(): #texte reuavecpisto + reusanspisto
        global clef,reunion
        reunion = 1
        clef = 1
        canvascle.create_image(0, 0, anchor=NW, image=clee)
        canvascle.place(x=570, y=699)
        playsound('Sons/grincement.wav')        
        if equipier == 1:
            text.config(text=reuavecpisto)
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state="normal", command=gauchegauche)
            boutonok.config(command=vide)
            playsound('Sons/cle.wav')
        if equipier == 0:
            text.config(text=reusanspisto)
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state="normal", command=salleequipier)
            boutongauche.config(state="normal", command=gauchegauche)
            boutonok.config(command=vide)
            playsound('Sons/cle.wav')
    def salleequipier(): #texte equiaveccle + equisanscle
        global pistolet,equipier
        equipier = 1
        pistolet = 1
        canvasflingue.create_image(0, 0, anchor=NW, image=flingue)
        canvasflingue.place(x=524.5, y=699)
        playsound('Sons/ouverture.wav')
        if reunion == 1:
            text.config(text=equiaveccle)
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state="normal", command=gauchegauche)
            boutonok.config(command=vide)
            playsound('Sons/casier.wav')
            playsound('Sons/pistolet.wav')
        if reunion == 0:
            text.config(text=equisanscle)
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state="normal", command=sallereunion)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state="normal", command=gauchegauche)
            boutonok.config(command=vide)
            playsound('Sons/casier.wav')
            playsound('Sons/pistolet.wav')
    def droit(): #texte droited
        remisemap8()
        playsound('Sons/pas.wav')
        text.config(text=droited)
        boutondroite.config(state="normal", command=droitdroit)
        boutonhaut.config(state="normal", command=local)
        boutonbas.config(state="normal", command=refectoire)
        boutongauche.config(state=DISABLED)
        boutonok.config(command=vide)
    def local(): #texte localavecpain + localsanspain
        global locale
        locale = 1
        playsound('Sons/grincement.wav') 
        if refectoir == 1:
            text.config(text=localavecpain)
            boutondroite.config(state="normal", command=droitdroit)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state=DISABLED)
            boutonok.config(command=vide)
        if refectoir == 0:
            text.config(text=localsanspain)
            boutondroite.config(state="normal", command=droitdroit)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state="normal", command=refectoire)
            boutongauche.config(state=DISABLED)
            boutonok.config(command=vide)
    def refectoire(): #texte refavecpain + refsanspain
        global refectoir,pain
        pain = 1
        refectoir = 1
        playsound('Sons/ouverture.wav')
        if locale == 1:
            text.config(text=refavecloc)
            boutondroite.config(state="normal", command=droitdroit)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state=DISABLED)
            boutonok.config(command=vide)
        if locale == 0:
            text.config(text=refsansloc)
            boutondroite.config(state="normal", command=droitdroit)
            boutonhaut.config(state="normal", command=local)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state=DISABLED)
            boutonok.config(command=vide)
        playsound('Sons/pain.wav')
    def gauchegauche(): #texte ascen
        remisemap3()
        playsound('Sons/pas.wav')
        text.config(text=ascen)
        playsound('Sons/ascenseur.wav')
        boutonhaut.config(state="normal", command=wcgauche)
        boutonbas.config(state=DISABLED)
        boutongauche.config(state=DISABLED)
        boutondroite.config(state=DISABLED)
        boutonok.config(command=vide)
    def wcgauche(): #texte wcgauchefirst
        remisemap4()
        global trousse
        trousse = 1
        playsound('Sons/pas.wav')
        text.config(text=wcgauchefirst)
        playsound('Sons/trousse.wav')
        canvastrousse.create_image(0, 0, anchor=NW, image=troussee)
        canvastrousse.place(x=612, y=699)
        boutondroite.config(state="normal", command=fou)
        boutonhaut.config(state=DISABLED)
        boutonbas.config(state=DISABLED)
        boutongauche.config(state=DISABLED) 
        boutonok.config(command=vide)
    def wcgauche2():
        global pain,trousse
        playsound('Sons/trousse.wav')
        if pain == 1:
            text.config(text="enplacement 4, wc fouillé, trouve une trousse, vous avez encore mal au ventre, utiliser la trousse ?")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state="normal", command=chercherpistolet2)
            boutongauche.config(state=DISABLED)
            boutonok.config(command=usetrousse) 
            pain = 0
        elif vie == 1:
            text.config(text="enplacement 4, wc fouillé, trouve une trousse, vous avez encore mal, utiliser la trousse ?")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state="normal", command=chercherpistolet2)
            boutongauche.config(state=DISABLED)
            boutonok.config(command=usetrousse)    
        else:
            canvastrousse.create_image(0, 0, anchor=NW, image=troussee)
            canvastrousse.place(x=612, y=699)
            trousse = 1
            text.config(text="enplacement 4, wc fouillé, trouve une trousse")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state="normal", command=chercherpistolet2)
            boutongauche.config(state=DISABLED)  
            boutonok.config(command=vide)      
    def fou(): #texte fou1 à 9
        remisemap5()
        playsound('Sons/pas.wav')
        playsound('Sons/chuchotement.wav')
        playsound('Sons/ouverture.wav')
        global pistolet,scalpel,fou,clef,trousse,vie
        if pistolet == 1 and scalpel == 0 and clef == 0:
            fou = 1
            text.config(text=fou1)
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state="normal", command=chercherclef)   
            boutonok.config(command=vide) 
            playsound('Sons/kill.wav')
        elif pistolet == 0 and scalpel == 1 and clef == 0:
            playsound('Sons/killcouteau.wav')
            fou = 1
            text.config(text=fou2)
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state="normal", command=chercherclef) 
            boutonok.config(command=vide)
        elif pistolet == 0 and scalpel == 1 and clef == 1:
            playsound('Sons/killcouteau.wav')
            text.config(text=fou3)
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state="normal", command=sortirfin)
            boutongauche.config(state=DISABLED)   
            boutonok.config(command=vide)
        elif pistolet == 1 and scalpel == 0 and clef == 1:
            text.config(text="Vous tuez le fou avec votre pistolet, vous pouvez sortir")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state="normal", command=sortirfin)
            boutongauche.config(state=DISABLED) 
            boutonok.config(command=vide)
            playsound('Sons/kill.wav')
        elif pistolet == 0 and scalpel == 0 and clef == 1 and trousse == 0:
            playsound('Sons/couppoing.wav')
            text.config(text="Vous entendez un bruit le fou vous saute dessus, vous perdez un point de vie trouvez une arme pour le tuer")
            boutondroite.config(state=NORMAL, command=lfscalpel)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state="normal", command=chercherpistolet) 
            boutonok.config(command=vide)
            canvasvie2.create_image(0, 0, anchor=NW, image=ptvie3)
            canvasvie2.place(x=680, y=750)
            vie = 1
        elif pistolet == 0 and scalpel == 0 and clef == 1 and trousse == 1:
            playsound('Sons/couppoing.wav')
            text.config(text="Vous entendez un bruit le fou vous saute dessus, vous perdez un point de vie trouvez une arme pour le tuer, vous avez une trousse utiliser ?")
            boutondroite.config(state=NORMAL, command=lfscalpel)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state="normal", command=chercherpistolet)
            boutonok.config(command=usetrousse)
            canvasvie2.create_image(0, 0, anchor=NW, image=ptvie3)
            canvasvie2.place(x=680, y=750)
            vie = 1
        elif pistolet == 0 and scalpel == 0 and clef == 0 and trousse == 0 :
            playsound('Sons/couppoing.wav')
            text.config(text="Vous entendez un bruit le fou vous saute dessus, vous perdez un point de vie trouvez une arme pour le tuer et la clef pour sortir")
            boutondroite.config(state=NORMAL, command=lfscalpel)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state="normal", command=chercherpistolet)   
            boutonok.config(command=vide) 
            canvasvie2.create_image(0, 0, anchor=NW, image=ptvie3)
            canvasvie2.place(x=680, y=750)
            vie = 1
        elif pistolet == 0 and scalpel == 0 and clef == 0 and trousse == 1 :
            playsound('Sons/couppoing.wav')
            text.config(text="Vous entendez un bruit le fou vous saute dessus, vous perdez un point de vie trouvez une arme pour le tuer\net la clef pour sortir, vous avez une trousse l'utiliser ? ok")
            boutondroite.config(state=NORMAL, command=lfscalpel)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state="normal", command=chercherpistolet) 
            boutonok.config(command=usetrousse)    
            canvasvie2.create_image(0, 0, anchor=NW, image=ptvie3)
            canvasvie2.place(x=680, y=750)
            vie = 1
    def lfscalpel():
        remisemap7()
        playsound('Sons/pas.wav')
        text.config(text="emplacement7, salle chirurgie")
        boutondroite.config(state="normal", command=chirurgie2)
        boutonhaut.config(state=DISABLED)
        boutonbas.config(state=DISABLED)
        boutongauche.config(state=DISABLED)
        boutonok.config(command=vide)
    def chercherpistolet():
        remisemap4()
        playsound('Sons/pas.wav')
        global pain,trousse,vie
        if pain == 1 and trousse == 0: 
            if vie == 0:
                playsound('Sons/douleur.wav')
                text.config(text="enplacement 4, wc gauche mal, perte point de vie")
                boutondroite.config(state=DISABLED)
                boutonhaut.config(state=DISABLED)
                boutonbas.config(state="normal", command=chercherpistolet2)
                boutongauche.config(state="normal", command=wcgauche2)
                boutonok.config(command=vide)
                canvasvie2.create_image(0, 0, anchor=NW, image=ptvie3)
                canvasvie2.place(x=680, y=750)
                vie=1
            if vie == 1:
                canvasviegauche.create_image(0, 0, anchor=NW, image=ptvie3)
                canvasviegauche.place(x=610, y=750) 
                playsound('Sons/douleur.wav')
                playsound('Sons/death.wav')
                text.config(text="Vous avez perdu toute votre vie, vous devez recommencer, appuyer sur ok")
                boutondroite.config(state=DISABLED)
                boutonhaut.config(state=DISABLED)
                boutonbas.config(state=DISABLED)
                boutongauche.config(state=DISABLED)
                boutonok.config(state="normal", command=quit)            

        elif pain == 0 and trousse == 0:
            text.config(text="enplacement 4, wc gauche pas mal")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state="normal", command=chercherpistolet2)
            boutongauche.config(state="normal", command=wcgauche2)
            boutonok.config(command=vide)
        else:
            text.config(text="enplacement 4, wc gauche pas mal")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state="normal", command=chercherpistolet2)
            boutongauche.config(state=DISABLED)    
            boutonok.config(command=vide) 
    def chercherpistolet2():
        remisemap2()
        playsound('Sons/pas.wav')
        global clef
        if clef == 1:
            text.config(text="emplacement 2,salle reu ou equipier")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state="normal", command=chercherpistolet3)
            boutongauche.config(state=DISABLED) 
            boutonok.config(command=vide)
        if clef == 0:
            text.config(text="emplacement 2,salle reu ou equipier")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=NORMAL, command=chercherclee)
            boutonbas.config(state="normal", command=chercherpistolet3)
            boutongauche.config(state=DISABLED)  
            boutonok.config(command=vide)
    def chercherclee():
        global clef
        clef = 1
        canvascle.create_image(0, 0, anchor=NW, image=clee)
        canvascle.place(x=570, y=699)
        playsound('Sons/grincement.wav')        
        playsound('Sons/cle.wav')
        if pistolet == 1:  
            text.config(text="emplacement 2,salle reu ou equipier")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state="normal", command=allertuerlefou)
            boutongauche.config(state=DISABLED) 
            boutonok.config(command=vide)
        else:
            text.config(text="emplacement 2,salle reu ou equipier")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state="normal", command=chercherpistolet3)
            boutongauche.config(state=DISABLED)  
            boutonok.config(command=vide)
    def allertuerlefou():
        text.config(text="vous avez trouver le pistolet et avez la clef, voulez retourner dans le dortoir ? ")
        boutondroite.config(state=DISABLED)
        boutonhaut.config(state=DISABLED)
        boutonbas.config(state=DISABLED)
        boutongauche.config(state=DISABLED) 
        boutonok.config(command=fou)                        
    def chercherpistolet3():
        global pistolet
        pistolet = 1
        playsound('Sons/ouverture.wav')
        playsound('Sons/casier.wav')
        playsound('Sons/pistolet.wav')
        canvasflingue.create_image(0, 0, anchor=NW, image=flingue)
        canvasflingue.place(x=524.5, y=699)
        if clef == 1:
            text.config(text="vous avez trouver le pistolet, voulez retourner dans le dortoir ? ")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state=DISABLED) 
            boutonok.config(command=fou)
        if clef == 0:
            text.config(text="emplacement 2,salle reu ou equipier")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=NORMAL, command=obligertrouverclef)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state=DISABLED)   
            boutonok.config(command=vide)
    def obligertrouverclef():
        global clef
        clef = 1
        canvascle.create_image(0, 0, anchor=NW, image=clee)
        canvascle.place(x=570, y=699)
        text.config(text="emplacement 2,salle reu ou equipier vous avez trouver la clef voulez vous retourner devant le dortoir ?")
        boutondroite.config(state=DISABLED)
        boutonhaut.config(state=DISABLED)
        boutonbas.config(state=DISABLED)
        boutongauche.config(state=DISABLED)     
        boutonok.config(state=NORMAL, command=fou)
        playsound('Sons/grincement.wav')
        playsound('Sons/cle.wav')
    def chercherclef():
        remisemap4()
        global pain,trousse,vie
        playsound('Sons/pas.wav')
        if pain == 1 and trousse == 0:
            if vie == 0:
                playsound('Sons/douleur.wav')
                text.config(text="enplacement 4, wc gauche aller chercher la clef perdez un pt de vie")
                boutondroite.config(state=DISABLED)
                boutonhaut.config(state=DISABLED)
                boutonbas.config(state="normal", command=chercherclef2)
                boutongauche.config(state=NORMAL, command=wcgauche3) 
                boutonok.config(command=vide)
                canvasvie2.create_image(0, 0, anchor=NW, image=ptvie3)
                canvasvie2.place(x=680, y=750)
            if vie == 1:
                canvasviegauche.create_image(0, 0, anchor=NW, image=ptvie3)
                canvasviegauche.place(x=610, y=750) 
                playsound('Sons/douleur.wav')
                text.config(text="Vous avez perdu toute votre vie, vous devez recommencer, appuyer sur ok")
                boutondroite.config(state=DISABLED)
                boutonhaut.config(state=DISABLED)
                boutonbas.config(state=DISABLED)
                boutongauche.config(state=DISABLED)
                boutonok.config(state="normal", command=quit)       
        elif pain == 0 and trousse == 0:
            text.config(text="enplacement 4, wc gauche pas mal")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state="normal", command=chercherclef2)
            boutongauche.config(state="normal", command=wcgauche3)
            boutonok.config(command=vide)
        elif pain == 0 and trousse == 1:
            text.config(text="enplacement 4, wc gauche pas mal")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state="normal", command=chercherclef2)
            boutongauche.config(state=DISABLED)
            boutonok.config(command=vide)
    def wcgauche3():
        global pain,trousse
        trousse = 1
        playsound('Sons/trousse.wav')
        canvastrousse.create_image(0, 0, anchor=NW, image=troussee)
        canvastrousse.place(x=612, y=699)
        if pain == 1:
            text.config(text="enplacement 4, wc fouillé, trouve une trousse, vous avez encore mal au ventre, utiliser la trousse ?")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state="normal", command=chercherclef2)
            boutongauche.config(state=DISABLED)
            boutonok.config(command=usetrousse) 
            pain = 0
        else:
            text.config(text="enplacement 4, wc fouillé, trouve une trousse")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state="normal", command=chercherclef2)
            boutongauche.config(state=DISABLED)  
            boutonok.config(command=vide)
    def usetrousse():
        global trousse,vie
        trousse = 0
        vie = 0
        playsound('Sons/usetrousse.wav')
        canvasvie2.create_image(0, 0, anchor=NW, image=ptvie1)
        canvasvie2.place(x=680, y=750)
        canvastrousse.destroy()
    def chercherclef2():
        remisemap2()
        playsound('Sons/pas.wav')
        global equipier
        text.config(text="emplacement 2,salle reu ou equipier")
        if equipier == 1:
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state="normal", command=chercherclef3)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state=DISABLED) 
            boutonok.config(command=vide)
        if equipier == 0:
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state="normal", command=chercherclef3)
            boutonbas.config(state=NORMAL, command=chercherflingue)
            boutongauche.config(state=DISABLED)   
            boutonok.config(command=vide)
    def chercherflingue():
        playsound('Sons/ouverture.wav')
        playsound('Sons/casier.wav')
        text.config(text="vous trouvez un pistolet mais vous avez deja tuer le fou vous ne le prenez donc pas, aller chercher la clef plutot")
        boutondroite.config(state=DISABLED)
        boutonhaut.config(state="normal", command=chercherclef3)
        boutonbas.config(state=DISABLED)
        boutongauche.config(state=DISABLED) 
        boutonok.config(command=vide)
    def chercherclef3():
        global pistolet,fou
        canvascle.create_image(0, 0, anchor=NW, image=clee)
        canvascle.place(x=570, y=699) 
        playsound('Sons/grincement.wav')
        playsound('Sons/cle.wav')
        if pistolet == 1 or scalpel == 1 and fou == 1:
            text.config(text="vous avez trouvez la clef, vous pouvez sortir")
            boutondroite.config(state="normal", command=sortirfin)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state=DISABLED)
            boutonok.config(command=vide)
        elif pistolet == 1 and fou == 0:
            text.config(text="vous avez trouvez la clef, vous devez tuer le fou , tp vers le fou ?")
            boutondroite.config(state=DISABLED)
            boutonhaut.config(state=DISABLED)
            boutonbas.config(state=DISABLED)
            boutongauche.config(state=DISABLED)
            boutonok.config(command=fou) 
    def sortirfin():
        remisemap1()   
        playsound('Sons/pas.wav')
        boutondroite.config(state=DISABLED)
        boutonhaut.config(state=DISABLED)
        boutonbas.config(state="normal", command=fin)
        boutongauche.config(state=DISABLED)
        boutonok.config(command=vide)
    def fin():
        remisemap0()
        playsound('Sons/pas.wav')
        playsound('Sons/sortie.wav')
        text.config(text="vous avez gagner bravo")
        boutondroite.config(state=DISABLED)
        boutonhaut.config(state=DISABLED)
        boutonbas.config(state=DISABLED)
        boutongauche.config(state=DISABLED)
        boutonok.config(state="normal", command=quit)
    def droitdroit():
        remisemap7()
        playsound('Sons/pas.wav')
        text.config(text="emplacement7, salle chirurgie")
        boutondroite.config(state="normal", command=chirurgie)
        boutonhaut.config(state="normal", command=wcdroit)
        boutonbas.config(state=DISABLED)
        boutongauche.config(state=DISABLED)
        boutonok.config(command=vide)
    def wcdroit():
        remisemap6()
        playsound('Sons/pas.wav')
        text.config(text="enplacement 6, wc droit vous ne trouvez rien")
        boutondroite.config(state=DISABLED)
        boutonhaut.config(state="normal", command=fou)
        boutonbas.config(state=DISABLED)
        boutongauche.config(state=DISABLED)
        boutonok.config(command=vide) 
    def chirurgie():
        global scalpel
        scalpel = 1
        canvascouteau.create_image(0, 0, anchor=NW, image=couteau)
        canvascouteau.place(x=524.5, y=699)
        playsound('Sons/ouverture.wav')
        playsound('Sons/takescalpel.wav')
        text.config(text="enplacement 7, salle chirurgie vous prenez le scalpel")
        boutonhaut.config(state="normal", command=wcdroit)
        boutonbas.config(state=DISABLED)
        boutongauche.config(state=DISABLED)
        boutondroite.config(state=DISABLED)
        boutonok.config(command=vide)
    def chirurgie2():
        global scalpel
        scalpel = 1
        canvascouteau.create_image(0, 0, anchor=NW, image=couteau)
        canvascouteau.place(x=524.5, y=699)
        playsound('Sons/takescalpel.wav')
        text.config(text="vous avez trouver un scalpel, voulez retourner dans le dortoir ? appuyer ok")
        boutondroite.config(state=DISABLED)
        boutonhaut.config(state=DISABLED)
        boutonbas.config(state=DISABLED)
        boutongauche.config(state=DISABLED) 
        boutonok.config(command=fou)
    #Mise en place des maps en fonction de l'emplacement
    def remisemap0():
        global canvasmap
        canvasmap.destroy()
        canvasmap = Canvas(jeu,width=850, height=550)
        canvasmap.create_image(0, 0, anchor=NW, image=map0)
        canvasmap.place(x=0, y=0)
    def remisemap1():
        global canvasmap
        canvasmap.destroy()
        canvasmap = Canvas(jeu,width=850, height=550)
        canvasmap.create_image(0, 0, anchor=NW, image=map1)
        canvasmap.place(x=0, y=0)
    def remisemap2():
        global canvasmap
        canvasmap.destroy()
        canvasmap = Canvas(jeu,width=850, height=550)
        canvasmap.create_image(0, 0, anchor=NW, image=map2)
        canvasmap.place(x=0, y=0)
    def remisemap3():
        global canvasmap
        canvasmap.destroy()
        canvasmap = Canvas(jeu,width=850, height=550)
        canvasmap.create_image(0, 0, anchor=NW, image=map3)
        canvasmap.place(x=0, y=0)
    def remisemap4():
        global canvasmap
        canvasmap.destroy()
        canvasmap = Canvas(jeu,width=850, height=550)
        canvasmap.create_image(0, 0, anchor=NW, image=map4)
        canvasmap.place(x=0, y=0)
    def remisemap5():
        global canvasmap
        canvasmap.destroy()
        canvasmap = Canvas(jeu,width=850, height=550)
        canvasmap.create_image(0, 0, anchor=NW, image=map5)
        canvasmap.place(x=0, y=0)
    def remisemap6():
        global canvasmap
        canvasmap.destroy()
        canvasmap = Canvas(jeu,width=850, height=550)
        canvasmap.create_image(0, 0, anchor=NW, image=map6)
        canvasmap.place(x=0, y=0)
    def remisemap7():
        global canvasmap
        canvasmap.destroy()
        canvasmap = Canvas(jeu,width=850, height=550)
        canvasmap.create_image(0, 0, anchor=NW, image=map7)
        canvasmap.place(x=0, y=0)
    def remisemap8():
        global canvasmap
        canvasmap.destroy()
        canvasmap = Canvas(jeu,width=850, height=550)
        canvasmap.create_image(0, 0, anchor=NW, image=map8)
        canvasmap.place(x=0, y=0)
    #Boutons
    boutonhaut = Button(jeu, image=flechehaut, relief=FLAT, highlightthickness=0, bg='black')
    boutonhaut.place(x=200, y=626)
    boutonbas = Button(jeu, image=flechebas, relief=FLAT, highlightthickness=0, bg='black')
    boutonbas.place(x=200, y=752.5)
    boutongauche = Button(jeu, image=flechegauche, relief=FLAT, highlightthickness=0, bg='black')
    boutongauche.place(x=145, y=695)
    boutondroite = Button(jeu, image=flechedroite, relief=FLAT, highlightthickness=0, bg='black')
    boutondroite.place(x=245, y=695)

    boutonok = Button(jeu, image=ok, text="Ok", relief=FLAT, command=premiertexte, highlightthickness=0, bg='black')
    boutonok.place(x=1000, y=675)

    jeu.mainloop()
#Ecran d'accueil
def accueilprincipal():
    global accueil
    accueil=Tk()
    accueil.title("Escape the Asylum")
    l=1280
    h=920
    largeur=accueil.winfo_screenwidth()
    hauteur=accueil.winfo_screenheight()
    x=(largeur/2)-(l/2)
    y=(hauteur/2)-(h/2)
    accueil.geometry("%dx%d+%d+%d" % (l,h,x,y))

    imageaccueil=PhotoImage(file="Image/load.png")

    canvasload = Canvas(accueil,width=1280, height=920)
    canvasload.create_image(0, 0, anchor=NW, image=imageaccueil)
    canvasload.pack()

    start = Button(accueil, text ='Jouer', command=jeuprinc, relief=FLAT, width=30, height= 5, bg="black", font='size, 10', fg="white")
    start2 = canvasload.create_window(400, 850, anchor=SE, window=start)

    quitter = Button(accueil, text ='Quitter', command=quit, relief=FLAT, width=30, height= 5, bg="black", font='size, 10', fg="white")
    quitter2= canvasload.create_window(550, 850, anchor=S, window=quitter)
    winsound.PlaySound('Sons/normal2.wav', winsound.SND_FILENAME|winsound.SND_ASYNC|winsound.SND_LOOP)
    accueil.mainloop()
accueilprincipal()