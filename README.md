# Jeux Python

Projet python - Jeux Escape the Asylum

Pour : Professeur (M. Guedj) et joueurs intéressés 

Besoin : Le joueur incarne un personnage qui va explorer le lieu d'une enquête et l'élucider car elle a été oublié par le commissariat de police  

C'est quoi : Escape The Asylum, jeu réalisé sous Python 3.7

Pourquoi le télécharger : gratuit, facile de prise en main, simple 

Alternative : Tout les autres jeux indépendants ou non

Notre garantie : Des mises à jours fréquentes pour améliorer le jeu (1 par mois)
